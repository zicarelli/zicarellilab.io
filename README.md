# zicarelli.dev

<https://zicarelli.dev>

## Workflow

1. Install [soupault].  If OPAM and OCaml are installed, you can do this by
   running: `opam install soupault`

2. To build, run the command: `soupault`

3. To publish, `git push` to: <https://gitlab.com/zicarelli/zicarelli.dev>

[soupault]: https://soupault.app
